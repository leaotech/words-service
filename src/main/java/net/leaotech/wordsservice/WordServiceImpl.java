package net.leaotech.wordsservice;

import java.util.Arrays;
import java.util.Comparator;

public class WordServiceImpl implements IWordService {

    @Override
    public String getLongest(String input) {
        return Arrays.stream(prepareInput(input).split(" "))
                     .max(Comparator.comparingInt(String::length))
                     .orElse(null);
    }

    @Override
    public String getShortest(String input) {
        return Arrays.stream(prepareInput(input).split(" "))
                     .min(Comparator.comparingInt(String::length))
                     .orElse(null);
    }

    public String getLongestAndLength(String input) {
        if (input != null && !input.isEmpty()) {
            String longest = getLongest(input);
            return String.format("%s and %d.", longest, longest.length());
        } else {
            return "";
        }
    }

    public String getShortestAndLength(String input) {
        if (input != null && !input.isEmpty()) {
            String shortest = getShortest(input);
            return String.format("%s and %d.", shortest, shortest.length());
        } else {
            return "";
        }
    }

    //removes commas, full stops etc from words
    private String prepareInput(String input) {
        if (input != null) {
            return input.replaceAll("[^a-zA-Z0-9_ -]", "");
        } else {
            return "";
        }
    }
}