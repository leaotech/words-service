package net.leaotech.wordsservice;

public interface IWordService {

    String getLongest(String input);

    String getShortest(String input);

    String getLongestAndLength(String input);

    String getShortestAndLength(String input);
}
