package net.leaotech.wordsservice

import spock.lang.Specification
import spock.lang.Unroll

class WordServiceImplTest extends Specification {

    IWordService underTest

    def setup(){
        underTest = new WordServiceImpl()
    }

    def "should return longest word and short word on a given sentence" () {
        given: 'an input String'
            def input = 'This phrase contains many words'

        /** longest **/
        when: 'the wordService get longest is invoked'
            def result = underTest.getLongest(input)
        then: 'the correct result is returned'
            result == 'contains'

        /** shortest **/
        when: 'the wordService getShortest is invoked'
            result = underTest.getShortest(input)
        then: 'the correct result is returned'
            result == 'This'

        and: 'and no exception is thrown'
            noExceptionThrown()
    }

    def "should return number of characters along with the longest/shortest word"() {
        given: 'an input String'
            def input = 'Just another phrase full of words'

        when:
            def result = underTest.getLongestAndLength(input)
        then: 'should return the longest string along with its length'
            result == "another and 7."

        when: 'the wordService toString method gets invoked'
            result = underTest.getShortestAndLength(input)
        then: 'should return the shortest string along with its length'
            result == "of and 2."
    }

    @Unroll
    def "catering for empty string and nulls"() {

        when: 'the method #method is invoked'
            def result = method
        then: 'should just return an empty string'
            result == expectedOutput
        and:
            noExceptionThrown()

        where:
            method                                               | expectedOutput
            ([] as WordServiceImpl).getLongest('')               | ""
            ([] as WordServiceImpl).getLongestAndLength('')      | ""
            ([] as WordServiceImpl).getShortest('')              | ""
            ([] as WordServiceImpl).getShortestAndLength('')     | ""
            ([] as WordServiceImpl).getLongest(null)             | ""
            ([] as WordServiceImpl).getLongestAndLength(null)    | ""
            ([] as WordServiceImpl).getShortest(null)            | ""
            ([] as WordServiceImpl).getShortestAndLength(null)   | ""

    }

    def "should return first occurrence of longest word"() {
        given: 'an input String'
            def input = 'word1 and word2 and word3 and word4'
        when: 'the wordService get longest is invoked'
            def result = underTest.getLongest(input)
        then: 'the correct result is returned'
            result == 'word1'
    }

    def "should return first occurrence of shortest word"() {
        given: 'an input String'
            def input = 'The cow jumped over the moon.'
        when: 'the wordService get shortest is invoked'
            def result = underTest.getShortest(input)
        then: 'the correct result is returned'
            result == 'The'
    }

    def "words with commas or full stop should'n interfere on the longest/shortest words logic"() {
        given: 'an input String'
            def input = 'The cow jumped over the rivers, then died'
        when: 'getLongest is invoked'
            def result = underTest.getLongest(input)
        then: 'the word jumped is returned as it is the first longest word with 6 letters instead of rivers,' +
                "note that the comma next to 'rivers,' did not affect the logic, still a 6 letters word"
            result == 'jumped'
    }
}
