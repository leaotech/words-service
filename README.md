##Getting Started

### Requirements
**Java 8**

### How to build
This uses gradle, the gradle wrapper is provided, please make sure the wrapper instead of your local gradle
 installation, this will ensure no build failures will happen.
 Build as follows so that all tests suites run:

unix based: `./gradlew clean build`

for the windows folks: `gradlew.bat clean build`

The above will generate a fat runnable jar: `build/libs/words-service-0.0.1-SNAPSHOT.jar`

The code was crafted on Intellij but should load just fine on Eclipse. When loading it into any of these IDEs, also
 ensure to select the provided gradle wrapper as the gradle distribution for the project.
 
###Assumptions:
Words don't contain special characters, only A-Z or/and 0-9/-_
  
####Steps to debug/browse the code on Intellij: 
* mark src/test/groovy as test source root (if not already set by the gradle plugin). This uses spock/groovy for all
 tests. This project has no main class, tests under src/test/groovy to be used to demonstrate the logic.